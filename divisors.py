#!/usr/bin/env python
# encoding: utf-8

import sys
import bisect

KNOWN_PRIMES = [ 2, 3 ]


# Returns the next prime for the given number <= KNOWN_PRIMES[-1].
# Adds found prime into KNOWN_PRIMES list if
# the list does not contain the it yet.
def getNextPrime(num):
    global KNOWN_PRIMES
    assert num <= KNOWN_PRIMES[-1]
    getNextPrime.calls += 1

    if num < KNOWN_PRIMES[0]:
        return KNOWN_PRIMES[0]

    p = KNOWN_PRIMES[-1]
    if num < p:
        return KNOWN_PRIMES[bisect.bisect(KNOWN_PRIMES, num)]

    for i in range(0, p, 2):
        succ = i + p
        for d in KNOWN_PRIMES:
            if succ % d == 0:
                break
        else:
            KNOWN_PRIMES.append(succ)
            return succ

    assert(False) # We should never reach this statement
getNextPrime.calls = 0

# Factorizes a number to its prime divisors.
# Returnes the list of prime divisors sorted by growth.
def factorize(number):
    assert number > 0

    factors = list()
    prime = 2
    num = int(number**0.5)

    while num >= prime:
        if number % prime == 0:
            factors.append(prime)
            number //= prime
            num = int(number**0.5)
        else:
            prime = getNextPrime(prime)

    factors.append(number)
    return factors


# Find required number by divisor amount
def getSmallestNumberFor(divisors):

    # Factorize given divisor_amount value
    factors = factorize(divisor_amount)
    print str(factors)

    prime = 0
    result = 1
    for f in reversed(factors):
        prime = getNextPrime(prime)
        result *= prime**(f-1)

    return result


# Execution start point
if len(sys.argv) != 2:
    sys.stderr.write(
        "Pass me desired divisors amount (a natural number)\n"
        "as an argument and nothing more!\n\n"
        )
    sys.exit(1)

try:
    divisor_amount = int(sys.argv[1])
except ValueError:
    sys.stderr.write("Illegal natural number passed\n\n")
    sys.exit(1)

if divisor_amount <= 0:
    sys.stderr.write("The natural number must be positive\n\n")
    sys.exit(1)

num = getSmallestNumberFor(divisor_amount)

print "The smallest natural number having\n" \
    "exactly %d divisors is %d" % ( divisor_amount, num )

print str(KNOWN_PRIMES)
print "getNextPrime was called %d times" % getNextPrime.calls
